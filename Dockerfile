FROM debian:bullseye-slim

WORKDIR /app
ADD bin/magick /usr/local/bin
ADD bin/ffmpeg /usr/local/bin
ADD bin/ffprobe /usr/local/bin
# ADD does some magic, like unpacking tar files...

ARG IM_VERSION="7.1.0-37"
ARG IM_BUILD_FLAGS="--enable-shared --disable-static --without-modules --enable-delegate-build --disable-docs --with-heic=yes"

ADD bin/svt-av1.tar.gz /usr/local/lib
RUN sed -i 's/bullseye main/bullseye main contrib non-free/' /etc/apt/sources.list && \
            apt-get update && \
            apt-get install -y \
            libjbig0 \
            liblcms2-2 \
            libjpeg62-turbo \
            liblqr-1-0 \
            libglib2.0 \
            libdjvulibre21 \
            libheif1 \
            libwebpmux3 \
            libwebpdemux2 \
            libopenexr25 \
            libopenjp2-7 \
            libgomp1 \
            libgnutls30 \
            libass9 \
            libdav1d4 \
            libaom0 \
            libfdk-aac2 \
            libmp3lame0 \
            libopus0 \
            libvorbis0a \
            libvorbisenc2 \
            libx264-160 \
            libx265-192 \
            libxcb1 \
            libxcb-shm0 \
            libvpx6 \
			build-essential \
            exiftool && \
            apt-get clean && \
            apt-get autoremove && \
            chmod +x /usr/local/bin/*


RUN apt-get update && apt-get install -y wget && \ 
    wget https://github.com/ImageMagick/ImageMagick/archive/refs/tags/${IM_VERSION}.tar.gz && \
    tar xzf ${IM_VERSION}.tar.gz && \
    rm ${IM_VERSION}.tar.gz
    
WORKDIR /app/ImageMagick-${IM_VERSION}
RUN sh ./configure ${IM_BUILD_FLAGS} && make -j && make install && ldconfig /usr/local/lib/

RUN apt-get update && apt-get install -y wget && \
	wget https://go.dev/dl/go1.21.3.linux-amd64.tar.gz && \
	rm -rf /usr/local/go && tar -C /usr/local -xzf go1.21.3.linux-amd64.tar.gz

ENV PATH=$PATH:/usr/local/go/bin



# FROM debian:bullseye-slim

# WORKDIR /app
# ADD bin/magick /usr/local/bin
# ADD bin/ffmpeg /usr/local/bin
# ADD bin/ffprobe /usr/local/bin
# # ADD does some magic, like unpacking tar files...

# ARG IM_VERSION="7.1.0-37"
# ARG IM_BUILD_FLAGS="--enable-shared --disable-static --without-modules --enable-delegate-build --disable-docs --with-heic=yes"

# ADD bin/svt-av1.tar.gz /usr/local/lib
# RUN sed -i 's/bullseye main/bullseye main contrib non-free/' /etc/apt/sources.list && \
#             apt-get update && \
#             apt-get install -y \
#             libjbig0 \
#             liblcms2-2 \
#             libjpeg62-turbo \
#             liblqr-1-0 \
#             libglib2.0 \
#             libdjvulibre21 \
#             libheif1 \
#             libwebpmux3 \
#             libwebpdemux2 \
#             libopenexr25 \
#             libopenjp2-7 \
#             libgomp1 \
#             libgnutls30 \
#             libass9 \
#             libdav1d4 \
#             libaom0 \
#             libfdk-aac2 \
#             libmp3lame0 \
#             libopus0 \
#             libvorbis0a \
#             libvorbisenc2 \
#             libx264-160 \
#             libx265-192 \
#             libxcb1 \
#             libxcb-shm0 \
#             libvpx6 \
# 			build-essential \
#             exiftool && \
#             apt-get clean && \
#             apt-get autoremove && \
#             chmod +x /usr/local/bin/* && \
#             apt-get install -y wget && \ 
#             wget https://github.com/ImageMagick/ImageMagick/archive/refs/tags/${IM_VERSION}.tar.gz && \
#             tar xzf ${IM_VERSION}.tar.gz && \
#             rm ${IM_VERSION}.tar.gz
    
# WORKDIR /app/ImageMagick-${IM_VERSION}
# RUN sh ./configure ${IM_BUILD_FLAGS} && \
#     make -j && make install && \
#     ldconfig /usr/local/lib/ && \
#     wget https://go.dev/dl/go1.18.3.linux-amd64.tar.gz && \
# 	rm -rf /usr/local/go && tar -C /usr/local -xzf go1.18.3.linux-amd64.tar.gz

# ENV PATH=$PATH:/usr/local/go/bin