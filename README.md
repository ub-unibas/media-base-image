# Media Base Image

Docker Base Image for Media Server and Indexer


This repository provides the means to build a Docker image which is used by the media server and the indexer as a base for media file processing. Namely, it provides a customised build of FFmpeg and ImageMagick as well as an Exiftool binary. FFmpeg and ImageMagick are built in two separate repositories:

- FFmpeg => https://gitlab.switch.ch/ub-unibas/ffmpeg-build
- ImageMagick => https://gitlab.switch.ch/ub-unibas/imagemagick-build

## Pull Latest Docker Image

```sh
docker pull cr.gitlab.switch.ch/ub-unibas/media-base-image:latest
```
